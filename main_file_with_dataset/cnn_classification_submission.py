import pandas as pd
from keras.layers import Dense, Dropout,Conv2D, Flatten
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint
from keras.models import Sequential,load_model
from keras.wrappers.scikit_learn import KerasClassifier
import keras.backend as K
from keras.layers import Dense
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score



def get_dataset():
    """
    :return: Get experimental raw train and test and submission data with dependent and independent features
    """
    # Get the raw train customer transaction data
    train = pd.read_csv('Data/train.csv')
    # Get the raw submission customer transaction data
    test = pd.read_csv('Data/test.csv')
    train['target'].value_counts(normalize=True)

    # get all the independent features from raw data
    independent_feat = ['var_' + str(i) for i in range(200)]
    # get the dependent feature from raw data
    dependent_feat = 'target'

    return train, test, independent_feat, dependent_feat

def scaled_features(train, test, independent_feat, dependent_feat):
    """
    Transform all the raw data for zero mean and unit variance
    :param train: train data with independent features variable
    :param test: submission data with independent features variable
    :param independent_feat: 200 independent features variable raw data
    :param dependent_feat:  1 dependent target feature variable raw data
    :return: Scaled training and testing data
    """

    sc = StandardScaler()

    # transform all the independent raw features training data
    train[independent_feat] = sc.fit_transform(train[independent_feat])

    # transform all the independent raw features submission data
    test[independent_feat] = sc.transform(test[independent_feat])

    # Split transformed data for training and testing
    train, testing_data = train_test_split(train, test_size=0.20, stratify=train[dependent_feat])

    # Get all the testing data as a csv format
    #get_test_data = testing_data
    #get_test_data.to_csv('testing_data.csv', index=False)

    return train, testing_data, test

def get_cnn_data(data):
    """
    :param data: one dimensional input data
    :return: four dimensional input data
    """
    return data.values.reshape(data.shape[0], 20, 10, 1)

def plot_history(history):
    """
    :param history: get the experimental results as a graph
    :return: training and validation score and loss from CNN model
    """

    # plot training and validation auc
    auc = history.history['get_auc']
    val_auc = history.history['val_get_auc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    x = range(1, len(auc) + 1)
    plt.plot(x, auc, 'b', label='Training AUC')
    plt.plot(x, val_auc, 'r', label='Validation AUC')
    plt.title('Training and validation AUC')
    plt.legend()
    plt.show()

    # plot training and validation loss
    plt.plot(x, loss, 'b', label='Training loss')
    plt.plot(x, val_loss, 'r', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend()
    plt.show()

def get_auc(y_true, y_pred):
    """

    :param y_true: actual data
    :param y_pred: predicted data
    :return: receiver operating characteristic score
    """
    print("True value {}".format(y_true))
    print("Predicted value {}".format(y_pred))
    auc = tf.metrics.auc(y_true, y_pred)[1]
    K.get_session().run(tf.local_variables_initializer())
    return auc

def cnn_model():
    """
        :return: trained CNN model
        """
    # get the sequential model from keras
    model = Sequential()

    # add model convolutional layers
    # convolutional layer 1
    model.add(Conv2D(64, kernel_size=3, activation='elu', input_shape=(20, 10, 1)))
    # convolutional layer 2
    model.add(Conv2D(32, kernel_size=3, activation='elu'))
    # convolutional layer 3
    model.add(Conv2D(16, kernel_size=3, activation='elu'))

    # add dropout layer for prevent overfitting
    model.add(Dropout(0.5))
    # flatten convolutional layer output data
    model.add(Flatten())
    # add dense layer 1
    model.add(Dense(1024, activation='elu'))
    # add dropout layer for prevent overfitting
    model.add(Dropout(0.5))
    # add dense layer 2
    model.add(Dense(512, activation='elu'))

    # output layer
    model.add(Dense(1, activation='sigmoid'))

    # compile model using accuracy to measure model performance
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=[get_auc])
    print(model.summary())
    return model

def main():

    # get training, testing and submission raw data with dependent and independent variable
    train, test, independent_feat, dependent_feat = get_dataset()

    print('Raw train data', train.head())
    print('Raw train data shape', train.shape)

    print("\n\n Raw Data Features")
    print(train[independent_feat].describe())

    train, testing_data, test= scaled_features(train, test, independent_feat, dependent_feat)

    # get the data for CNN model input
    train_cnn = get_cnn_data(train[independent_feat])
    testing_data_cnn = get_cnn_data(testing_data[independent_feat])
    test_cnn = get_cnn_data(test[independent_feat])

    print('\n\nScaled train data shape', train.shape)
    print('Scaled test data shape', testing_data.shape)

    # get the model path
    mode_path = 'cnn_mode_path.h5'
    callbacks = [ModelCheckpoint(filepath=mode_path, save_best_only=True)]



    ## Run Model
    print("\n\nCompile model ...")
    estimator = KerasClassifier(build_fn=cnn_model, epochs = 20, batch_size=64)
    history = estimator.fit(train_cnn, train[dependent_feat], validation_data=(testing_data_cnn, testing_data[dependent_feat]),
                            callbacks=callbacks)



    # get the plot for training and validation auc from CNN model
    plot_history(history)

    model = load_model(mode_path, custom_objects={'get_auc': get_auc})

    # predicting the train data during the model training
    train_pred = model.predict_proba(train_cnn)
    testing_data_pred = model.predict_proba(testing_data_cnn)


    train_pred = [i[0] for i in train_pred]
    testing_data_pred = [i[0] for i in testing_data_pred]

    print("Training auc {}".format(roc_auc_score(train[dependent_feat], train_pred)))
    print("Validation auc {}".format(roc_auc_score(testing_data[dependent_feat], testing_data_pred)))



    #result = pd.DataFrame({'ID_code': val['ID_code'], 'target': list(testing_data_pred)})
    #result.to_csv('testing_result.csv', index=False)

    ''' Get the submission data prediction results from trained CNN model'''
    test_pred = model.predict_proba(test_cnn)
    test_pred = [i[0] for i in test_pred]

    submission_result = pd.DataFrame({'ID_code': test['ID_code'], 'target': list(test_pred)})
    submission_result.head()
    submission_result.to_csv('submission_data.csv', index=False)


if __name__=="__main__":
    main()